# Prototype design 2

No central broker. We have _herds_ of "listener processes" organized in clusters for more reliability. A group of
listeners has the following role:
1. They listen to subscriptions on a `Device/Parameter` (to be defined how);
2. When a new subscription request arrives, they **also** subscribe to `Device/Parameter` (they subscribe all together
    to make sure we always have at least one listening).
3. The client receives data directly from the device server hosting `Device/Parameter`, therefore there's no additional
    latency.
4. The listener herd stores the listened data in some fast local log/database (Redis?)
5. If the client stops the subscription gracefully, the listener herd drops the subscriptions and everything they have
    stored in the local log.
6. If the client does not send pings for some time, the next time it reconnects it should talk to Redis to recover the
    missed messages (to be defined how to make the switch).

Questions:
1. How to make the switch at step 6?
2. How to detect that the client disconnected ungracefully?
3. How to listen for incoming subscriptions on a specific `Device/Parameter` from the outside? (i.e. from a K8s cluster
   in another machine)

Some ideas:
1. The first connection could always be towards one of the listener herds. Then the herd is responsible for instructing
    the appropriate device server where to send data (therefore no additional hop). When the client reconnects it
    should be smart enough to understand that it's possible something was lost, thus it talks again to the listener
    herd.
2. If we adopt the solution above, the client is responsible to understand that it may have lost a few messages.
3. Again, if we adopt the solution above this is not a problem.
