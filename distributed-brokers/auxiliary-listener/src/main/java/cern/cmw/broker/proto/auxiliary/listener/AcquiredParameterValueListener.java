package cern.cmw.broker.proto.auxiliary.listener;

import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.ParameterException;
import cern.japc.core.ParameterValueListener;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class AcquiredParameterValueListener implements ParameterValueListener {

    @NonNull
    private final StorageService storageService;

    @Override
    public void valueReceived(String parameterName, AcquiredParameterValue value) {
        log.info("Value received for '{}'", parameterName);
        storageService.store(value);
    }

    @Override
    public void exceptionOccured(String parameterName, String description, ParameterException exception) {
        log.error("Error for '{}': {}", parameterName, description, exception);
    }

}
