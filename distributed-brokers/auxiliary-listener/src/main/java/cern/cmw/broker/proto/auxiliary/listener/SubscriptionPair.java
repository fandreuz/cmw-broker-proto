package cern.cmw.broker.proto.auxiliary.listener;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class SubscriptionPair implements Comparable<SubscriptionPair> {

    @NonNull
    private final String parameterName;
    @NonNull
    private final String selectorId;

    @Override
    public int compareTo(@NotNull SubscriptionPair subscriptionPair) {
        return getParameterName().compareTo(subscriptionPair.getParameterName());
    }
}
