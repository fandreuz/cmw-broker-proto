package cern.cmw.broker.proto.auxiliary.listener;

import cern.japc.core.AcquiredParameterValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Subscriber implements Comparable<Subscriber> {

    @NonNull
    private String updateAddress;

    @Override
    public int compareTo(@NotNull Subscriber subscriber) {
        return updateAddress.compareTo(subscriber.getUpdateAddress());
    }

    public void sendUpdate(@NonNull AcquiredParameterValue value) {

    }

}
