package cern.cmw.broker.proto.auxiliary.listener;

import cern.japc.core.AcquiredParameterValue;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

@Component
public interface StorageService {

    void store(@NonNull AcquiredParameterValue value);
    Stream<AcquiredParameterValue> replay(long exclusiveLowerBoundMillis, @NonNull SubscriptionPair subscriptionPair);

}
