package cern.cmw.broker.proto.auxiliary.listener;

import lombok.NonNull;

public class ClientNotRegisteredException extends RuntimeException {

    public ClientNotRegisteredException(@NonNull String message) {
        super(message);
    }

}
