package cern.cmw.broker.proto.auxiliary.listener;

import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.ParameterRuntimeException;
import cern.japc.core.Parameters;
import cern.japc.core.SubscriptionHandle;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.factory.SelectorFactory;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Controller
public class SubscriptionController {

    private static final ParameterFactory factory = ParameterFactory.newInstance();

    private final ConcurrentHashMap<SubscriptionPair, SubscriptionHandle> activeSubscriptions = new ConcurrentHashMap<>();
    private final SortedSetMultimap<SubscriptionPair, Subscriber> subscribers = Multimaps
            .synchronizedSortedSetMultimap(TreeMultimap.create());

    @Autowired
    private StorageService storageService;

    @PostMapping("/subscribe")
    public void subscribe( //
            @RequestParam String device, //
            @RequestParam String property, //
            @RequestParam String selectorId, //
            @RequestParam Optional<Long> lastAcqStampMillis, //
            // TODO HTTP, gRPC, ...
            @RequestBody String subscriberAddress //
    ) {
        String parameterName = Parameters.buildParameterName(device, property);
        var subscriptionPair = new SubscriptionPair(parameterName, selectorId);

        var subscriber = new Subscriber(subscriberAddress);
        // TODO No computeIfAbsent for Guava?
        if (lastAcqStampMillis.isEmpty()) {
            subscribers.put(subscriptionPair, subscriber);
        } else {
            if (!subscribers.get(subscriptionPair).contains(subscriber)) {
                String msg = String.format("The client '%s' wasn't found in the registry", subscriber);
                throw new ClientNotRegisteredException(msg);
            }

            // Client is trying to reconnect, let's deliver lost data
            storageService.replay(lastAcqStampMillis.get(), subscriptionPair) //
                    .forEachOrdered(subscriber::sendUpdate);
        }

        activeSubscriptions.computeIfAbsent(subscriptionPair, this::startSubscription);
    }

    private SubscriptionHandle startSubscription(@NonNull SubscriptionPair pair) {
        Parameter parameter;
        try {
            parameter = factory.newParameter(pair.getParameterName());
        } catch (ParameterException exception) {
            throw new ParameterRuntimeException("Invalid parameter", exception);
        }
        var selector = SelectorFactory.newSelector(pair.getSelectorId());

        var handle = parameter.createSubscription(selector, new AcquiredParameterValueListener(storageService));
        try {
            handle.startMonitoring();
        } catch (ParameterException exception) {
            throw new ParameterRuntimeException(exception);
        }

        log.info("Added new subscription: {}", pair);
        return handle;
    }

}
