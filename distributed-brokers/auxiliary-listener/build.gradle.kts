import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("org.springframework.boot")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-web:2.7.10")
    implementation("cern.japc:japc:7.14.2")
    implementation("cern.japc:japc-ext-cmwrda3:7.14.0")
    implementation("com.google.guava:guava:32.1.2-jre")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}
