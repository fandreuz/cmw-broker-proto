package cern.cmw.broker.proto.rda.source;

import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.ParameterException;
import cern.japc.core.ParameterValueListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link ParameterValueListener} for publication to Kafka.
 * <p>
 * This class published messages to Kafka along with a key. The parameter name
 * is used, therefore updates from the same parameter are guaranteed to go to
 * the same partition (see
 * {@link org.apache.kafka.clients.producer.internals.DefaultPartitioner}.
 *
 * @author fandreuz
 */
@Slf4j
@Component
public class KakaPublishingParameterValueListener implements ParameterValueListener {

    @Autowired
    private KafkaTemplate<String, Double> template;

    @Override
    public void valueReceived(String parameterName, AcquiredParameterValue value) {
        log.info("Value for '{}'", parameterName);
        template.send( //
                template.getDefaultTopic(), //
                parameterName, //
                value.getValue().castAsMap().getDouble("average") //
        );
    }

    @Override
    public void exceptionOccured(String parameterName, String description, ParameterException exception) {
        log.error("Exception for '{}'", parameterName, exception);
    }

}
