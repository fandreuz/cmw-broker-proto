package cern.cmw.broker.proto.rda.source;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Configuration component for Kafka broker.
 *
 * @author fandreuz
 */
@Component
@Slf4j
public class KafkaConfiguration {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;
    @Value(value = "${cmw.broker.proto.topic.name}")
    private String topicName;
    @Value(value = "${cmw.broker.proto.topic.partitions.count:1}")
    private int partitionsCount;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        log.info("Broker address: {}", bootstrapAddress);
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic rdaTopic() {
        return TopicBuilder.name(topicName) //
                .partitions(partitionsCount) //
                // TODO
                .replicas(1) //
                .build();
    }
}
