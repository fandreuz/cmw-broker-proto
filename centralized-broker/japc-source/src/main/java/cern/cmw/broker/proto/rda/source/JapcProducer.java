package cern.cmw.broker.proto.rda.source;

import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.ParameterValueListener;
import cern.japc.core.Selectors;
import cern.japc.core.SubscriptionHandle;
import cern.japc.core.factory.ParameterFactory;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Simple producer based on JAPC subscriptions.
 * <p>
 * TODO:
 * <ul>
 * <li>Support selectors configuration</li>
 * <li>Support heterogeneous data (via maps?)</li>
 * </ul>
 *
 * @author fandreuz
 */
@Component
@Slf4j
public class JapcProducer {

    private static final ParameterFactory parameterFactory = ParameterFactory.newInstance();

    @Value("#{'${cmw.broker.proto.parameters}'.split(',')}")
    private List<String> parameters;

    @Autowired
    private ParameterValueListener listener;

    private Map<String, SubscriptionHandle> subscriptionHandles;

    @PostConstruct
    void runner() {
        log.info("Initializing subscriptions... (CMW/RBAC env = {}/{})", System.getProperty("cmw.directory.env"),
                System.getProperty("rbac.env"));
        subscriptionHandles = parameters.stream() //
                .map(JapcProducer::initializeParameter) //
                .filter(Optional::isPresent) //
                .map(Optional::get) //
                .collect(Collectors.toMap( //
                        Parameter::getName, //
                        parameter -> parameter.createSubscription( //
                                // TODO
                                Selectors.NO_SELECTOR, //
                                listener //
                        ) //
                ));
        subscriptionHandles.values().forEach(JapcProducer::startMonitoring);
    }

    private static Optional<Parameter> initializeParameter(@NonNull String parameter) {
        try {
            return Optional.of(parameterFactory.newParameter(parameter));
        } catch (ParameterException exception) {
            log.error("Parameter factory: error occurred for {}", parameter, exception);
            return Optional.empty();
        }
    }

    private static void startMonitoring(@NonNull SubscriptionHandle subscriptionHandle) {
        try {
            subscriptionHandle.startMonitoring();
        } catch (ParameterException exception) {
            String parameterName = subscriptionHandle.getParameter().getName();
            log.error("An exception occurred while starting monitoring {}", parameterName, exception);
        }
    }

    @PreDestroy
    void tearDown() {
        if (subscriptionHandles != null) {
            subscriptionHandles.values().forEach(SubscriptionHandle::stopMonitoring);
            subscriptionHandles.clear();
        }
        subscriptionHandles = null;
    }

}
