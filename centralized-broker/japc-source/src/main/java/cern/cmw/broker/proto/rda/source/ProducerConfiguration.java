package cern.cmw.broker.proto.rda.source;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.DoubleSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Configuration component for Kafka Producer.
 *
 * @author fandreuz
 */
@Component
public class ProducerConfiguration {

    @Autowired
    private NewTopic newTopic;

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    private Map<String, Object> producerConfigs() {
        return Map.of( //
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress, //
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class, //
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, DoubleSerializer.class //
        );
    }

    @Bean
    public KafkaTemplate<String, Double> kafkaTemplate() {
        var template = new KafkaTemplate<String, Double>(new DefaultKafkaProducerFactory<>(producerConfigs()));
        template.setDefaultTopic(newTopic.name());
        return template;
    }

}
