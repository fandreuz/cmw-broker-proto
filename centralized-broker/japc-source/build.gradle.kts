import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("org.springframework.boot")
}

dependencies {
    implementation("org.springframework.kafka:spring-kafka:2.8.9")
    implementation("org.springframework.boot:spring-boot-starter-web:2.7.10")
    implementation("cern.japc:japc:7.14.2")
    implementation("cern.japc:japc-ext-cmwrda3:7.14.0")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.named<BootRun>("bootRun") {
    systemProperties = mapOf(
            Pair("rbac.env", "TEST"), //
            Pair("cmw.directory.env", "TEST"), //
            // Kafka configuration
            Pair("spring.kafka.bootstrap-servers", "localhost:9092"), //
            // cmw.broker.proto configuration
            Pair("cmw.broker.proto.topic.name", "DEV1"), //
            Pair("cmw.broker.proto.parameters",
                    "UCAP.TESTBED.0050/Acquisition,UCAP.TESTBED.0051/Acquisition,UCAP.TESTBED.0052/Acquisition" //
            )
    )
}
