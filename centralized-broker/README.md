# Prototype design

The goal is divided in two fully independent steps, collecting data from the producers to the brokers, and distributing
downstream the data saved in the Brokers. The design should also touch the configuration of the Brokers.

## Gathering data

- Some sources are hard to modify and redeploy, in these cases we need to pull data via RDA/JAPC to push it to the
    broker
  - See `japc-source` project; 
  - The recommended solution for these cases is *Kafka Connect*, but it looks convoluted to set up and requires some
        coding to implement the needed features, therefore it's probably something to look into after the prototype
        phase.
  - Another possibly coexistent solution is to manage a K8s cluster of Kafka Producers.
- Other sources (e.g. UCAP) are more flexible and could benefit from an embedded Kafka producer
  - Benefits: robustness and throughput (one less layer to reach the broker), if the process fails we can safely assume
        no data is being produced while the producer thread stays offline.
  - Could be implemented as some kind of generic decorator/wrapper on the device server implementation, but should
        not require too much work to generalize for other projects.

To summarize, I expect in the long run we could have many sources publishing to the brokers to model different 
producers: Kafka Producers, both embedded and standalone, and Kafka Connect tasks. For the prototype phase we'll only
focus on Kafka Producers.

## Brokers

In-order guarantee is only provided for data within a single partition, therefore data produced by a single 
`Device/Parameter` pair should stay in the same partition. Topics should be related to a single team/project.

Keys (`Device/Parameter` in our case) are not related to the offset, therefore if a client is interested in
`Device1/Parameter1` at some point, and later becomes interested in `Device1/Parameter2` which happens to be logged in
the same partition as `Device1/Parameter1`, the client would lose messages related to `Device1/Parameter2` since the
offset was moved. For this reason we could consider appending the `Device/Parameter` pair to the consumer group ID, 
such that each client can independently track different parameters even if they are stored in the same partition.

The paragraph above implies that each Kafka Consumer should be responsible for one single `Device/Parameter` pair. If
this wasn't the case, messages from two pairs would be read, and any subsequent individual read of one the parameters
would cause duplicate messages to be delivered.

## Distributing data

Different clients should be part of different consumer groups, such that Kafka considers them as fully independent. 
Clients should subscribe to a topic and request a particular `Device/Parameter` pair. Kafka will also handle failures,
making sure the current offset is stored safely for each subscribed client.

For the prototype phase we model clients as simple Kafka producers.

## Discussion with Marcin

Each subscription should go to a single topic, and we should make sure that each subscription goes to the same 
partition, to guarantee ordering. We cannot have an infinite amount of topics, therefore we will need to put many
subscriptions to the same topic. This means a bigger burden on the client, which is forced to consume and filter the
messages to retain only what they need. Also, the client would have to consume the same messages many times from the
beginning in case it subscribes to many subscriptions from the same topic/partition.

Another concern is the latency. We're adding an additional hop to the architecture, some application may not be able to
couple with it.
