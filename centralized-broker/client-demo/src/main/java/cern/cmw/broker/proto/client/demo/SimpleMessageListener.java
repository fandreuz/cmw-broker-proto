package cern.cmw.broker.proto.client.demo;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.MessageListener;

/**
 * Simple message listener. Logs key/value pairs.
 *
 * @author fandreuz
 */
@Slf4j
@AllArgsConstructor
public final class SimpleMessageListener implements MessageListener<String, Double> {

    private final int messageListenerId;

    @Override
    public void onMessage(ConsumerRecord<String, Double> kafkaRecord) {
        log.info("[{}] Message: key={}, value={}", messageListenerId, kafkaRecord.key(), kafkaRecord.value());
    }

}
