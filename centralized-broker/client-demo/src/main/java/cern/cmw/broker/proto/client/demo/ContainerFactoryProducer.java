package cern.cmw.broker.proto.client.demo;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.DoubleDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Component to produce container factories.
 *
 * @author fandreuz
 */
@Component
@Slf4j
public class ContainerFactoryProducer {

    @Value(value = "${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    private ConsumerFactory<String, Double> consumerFactory() {
        Map<String, Object> props = Map.of( //
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress, //
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class, //
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, DoubleDeserializer.class //
        );
        return new DefaultKafkaConsumerFactory<>(props);
    }

    public ConcurrentKafkaListenerContainerFactory<String, Double> containerFactory(@NonNull String parameter) {
        ConcurrentKafkaListenerContainerFactory<String, Double> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }

}
