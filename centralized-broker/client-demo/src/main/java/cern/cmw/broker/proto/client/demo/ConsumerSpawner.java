package cern.cmw.broker.proto.client.demo;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.adapter.FilteringMessageListenerAdapter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Takes care of mapping parameter declared in
 * {@code cmw.broker.proto.parameters} to Kafka consumers.
 *
 * @author fandreuz
 */
@EnableKafka
@Component
@Slf4j
public class ConsumerSpawner {

    @Value(value = "${cmw.broker.proto.topic.name}")
    private String topicName;
    @Value(value = "${cmw.broker.proto.consumer.id}")
    private String consumerId;
    @Value("#{'${cmw.broker.proto.parameters}'.split(',')}")
    private List<String> parameters;

    @Autowired
    private ContainerFactoryProducer containerFactoryProducer;

    @Bean
    public Set<ConcurrentMessageListenerContainer<String, Double>> listenerContainers() {
        return IntStream.range(0, parameters.size()) //
                .mapToObj(index -> {
                    String parameter = parameters.get(index);
                    var container = containerFactoryProducer.containerFactory(parameter) //
                            .createContainer(topicName);

                    var messageListener = new SimpleMessageListener(index);
                    container.getContainerProperties().setMessageListener(new FilteringMessageListenerAdapter<>( //
                            messageListener, //
                            consumerRecord -> !consumerRecord.key().equals(parameter)) //
                    );

                    String groupId = buildGroupId(parameter);
                    container.getContainerProperties().setGroupId(groupId);
                    container.setBeanName(groupId);

                    container.start();
                    return container;
                }) //
                .collect(Collectors.toSet());
    }

    private String buildGroupId(@NonNull String parameter) {
        return String.format("%s:%s", consumerId, parameter);
    }

}
