package cern.cmw.broker.proto.client.demo;

import cern.cmw.broker.proto.rda.source.KafkaConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Main class for the client-demo subproject.
 *
 * @author fandreuz
 */
@SpringBootApplication
@Import(KafkaConfiguration.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
