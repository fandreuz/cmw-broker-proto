import org.springframework.boot.gradle.tasks.run.BootRun

plugins {
    id("org.springframework.boot")
}

dependencies {
    implementation(project(":japc-source"))
    implementation("org.springframework.kafka:spring-kafka:2.8.9")
    implementation("org.springframework.boot:spring-boot-starter-web:2.7.10")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

val defaultBootRun = tasks.named<BootRun>("bootRun").get()

tasks.create<BootRun>("runConsumer1") {
    mainClass.set(defaultBootRun.mainClass)
    classpath(defaultBootRun.classpath)

    systemProperties = mapOf(
            // Kafka configuration
            Pair("spring.kafka.bootstrap-servers", "localhost:9092"), //
            // cmw.broker.proto configuration
            Pair("cmw.broker.proto.topic.name", "DEV1"), //
            Pair("cmw.broker.proto.parameters", "UCAP.TESTBED.0050/Acquisition,UCAP.TESTBED.0051/Acquisition"), //
            Pair("cmw.broker.proto.consumer.id", "consumer1"), //
            // To prevent conflicts
            Pair("server.port", 8081)
    )
}

tasks.create<BootRun>("runConsumer2") {
    mainClass.set(defaultBootRun.mainClass)
    classpath(defaultBootRun.classpath)

    systemProperties = mapOf(
            // Kafka configuration
            Pair("spring.kafka.bootstrap-servers", "localhost:9092"), //
            // cmw.broker.proto configuration
            Pair("cmw.broker.proto.topic.name", "DEV1"), //
            Pair("cmw.broker.proto.parameters", "UCAP.TESTBED.0051/Acquisition,UCAP.TESTBED.0052/Acquisition"), //
            Pair("cmw.broker.proto.consumer.id", "consumer2"), //
            // To prevent conflicts
            Pair("server.port", 8082)
    )
}
