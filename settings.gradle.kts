rootProject.name = "cmw-broker-proto"

include("japc-source")
project(":japc-source").projectDir = File("centralized-broker/japc-source")
include("client-demo")
project(":client-demo").projectDir = File("centralized-broker/client-demo")

include("auxiliary-listener")
project(":auxiliary-listener").projectDir = File("distributed-brokers/auxiliary-listener")
