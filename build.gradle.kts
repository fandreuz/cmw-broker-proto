plugins {
    `java-library`
    id("com.diffplug.spotless") version "6.11.0"
    id("io.freefair.lombok") version "8.0.1"
    id("org.springframework.boot") version "2.7.4" apply false
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

spotless {
    java {
        target(fileTree(".") {
            include("**/*.java")
            exclude("**/build/**", "**/build-*/**")
        })
        toggleOffOn()
        eclipse()
        removeUnusedImports()
        trimTrailingWhitespace()
        endWithNewline()
        indentWithSpaces(4)
    }
    kotlin {
        target(fileTree(".") {
            include("**/*.kts")
            exclude("**/build/**", "**/build-*/**")
        })
        toggleOffOn()
        trimTrailingWhitespace()
        endWithNewline()
    }

}

allprojects {
    repositories {
        mavenCentral()
    }
    apply(plugin = "java-library")
    apply(plugin = "io.freefair.lombok")
}
