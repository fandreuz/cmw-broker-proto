# cmw-broker-proto

Prototype broker-based `cmw-rda` implementation. Material for Francesco's MSc thesis.

## Spring properties

+ `spring.kafka.bootstrap-servers`: URL to the Kafka Cluster. Not stored in VCS since for now the cluster is started locally.

### Producer

Spring properties available in producers (e.g. `japc-source`):

+ `cmw.broker.proto.topic.name`: Target topic name;
+ `cmw.broker.proto.parameters`: Commma-separated `Device/Parameter` pairs monitored by this producer.

### Consumer

Spring properties available in consumers (e.g. `client-demo`):

+ `cmw.broker.proto.topic.name`: Listened topic name;
+ `cmw.broker.proto.parameters`: Commma-separated `Device/Parameter` pairs monitored by this consumer;
+ `cmw.broker.proto.consumer.id`: Kafka consumer ID.

## Consumer demo

The consumer catches up with all the lost messages after it comes back online:

```
2023-08-08 22:20:57.880  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954027.0
2023-08-08 22:20:59.076  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954028.0
2023-08-08 22:20:59.083  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954028.0
2023-08-08 22:21:00.294  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954029.0
2023-08-08 22:21:00.295  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954029.0
2023-08-08 22:21:01.476  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954030.0
2023-08-08 22:21:01.484  INFO 9994 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954030.0
^C
... (some time elapsed)
2023-08-08 22:21:47.152  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954031.0
2023-08-08 22:21:47.152  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954031.0
2023-08-08 22:21:47.153  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954032.0
2023-08-08 22:21:47.153  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954032.0
2023-08-08 22:21:47.153  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954033.0
2023-08-08 22:21:47.153  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [1] Message: key=UCAP.TESTBED.0051/Acquisition, value=954033.0
2023-08-08 22:21:47.153  INFO 10989 --- [quisition-0-C-1] c.c.b.p.c.demo.SimpleMessageListener     : [0] Message: key=UCAP.TESTBED.0050/Acquisition, value=954034.0
```

## Tools

- Java
- Spring/Spring Boot
- Apache Kafka
- Gradle