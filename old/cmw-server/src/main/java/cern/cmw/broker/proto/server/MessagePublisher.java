package cern.cmw.broker.proto.server;

import cern.cmw.broker.proto.common.Configuration;
import java.time.Instant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessagePublisher {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Scheduled(fixedDelay = 1000)
    public void sendMessage() {
        kafkaTemplate.send(Configuration.TOPIC_NAME, "Msg at t=" + Instant.now().getEpochSecond()) //
                .addCallback( //
                        result -> log.info("Sent a message to topic '{}'", Configuration.TOPIC_NAME), //
                        exception -> log.error("An error occurred while sending a message to topic '{}'",
                                Configuration.TOPIC_NAME, exception));
    }
}
