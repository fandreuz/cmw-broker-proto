dependencies {
    implementation(project(":cmw-common"))
    implementation("org.springframework.boot:spring-boot-starter:" + project.properties["springBootVersion"])
    implementation("org.springframework.kafka:spring-kafka:" + project.properties["springKafkaVersion"])
}
