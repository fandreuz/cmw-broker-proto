# Architecture

Some ideas about the architecture of the prototype.

## Idea 1 (discarded due to architectural problems)

CMW server map to Kafka topics, devices/properties are filtered by using the key.

A CMW client is a Kakfa client. Clients consume from the appropriate topics. We benefit from Kafka built-in failover support to retain reading offset inside Kafka log.

Dropped because apparently Kafka does too much internal magic, consumers are not really lightweight to create and influence the balancing of other consumers.

## Idea 1b

Like _Idea 1_, but each CMW clients is a Kafka consumer having its own consumer group, such that it does not perturb other clients. To be seen whether this gives problems in terms of performance, but we would get offset persistence out of the box.

## Idea 2 (too complicated and feels fishy)

### High level idea

CMW server gets splitted into two parts: a Kafka producer to push data to the broker, and a Kafka consumer to handle reliably communication with the clients. In the middle we put a Kafka cluster to persist events.

Clients _pull_ data from Kafka consumers (the appropriate consumer is selected depending on the machine/device/property triple). Each _pull_ carries the offset, therefore the offset is always provided to the client. Kafka consumers use the offset to extract the appropriate event from the cluster.

### Consumers

Consumers are Kubernetes pods, therefore we get failover and routing via Ingress. One consumer per machine (i.e. one per CMW server).

The consumer is responsible of:

1. Extracting needed data from the broker leveraging the offset provided by the client.
1. Sending back extracted data to the pulling client.
1. Taking notes of interest in device/property pairs (coming from CMW clients) for pre-fetching purposes.

If no offset is provided together with the _pull_ request, the consumer should just assume that this is the first connection for the client.

Filtering by selector could be done on the consumer.

### CMW clients

Clients are standalone processes decoupled from Kafka consumers. Clients _pull_ from Kafka consumers, and pass along the current offset in the log.

Clients could store the offset in a separate Kafka topic, providing also a unique ID to distinguish between clients.

### Questions

Q: Do we really need the middleware of consumers? What's the added value?
A: Always-on connection to the cluster (to be checked if it's expensive to establish), stateless, runs on Kubernetes therefore we get Service Discovery and failover out of the box.
Q: It could be simpler if clients were just consumers.

```plantuml
@startuml
left to right direction
title Idea 2

card Kubernetes {
    agent "machine-a-consumer" as mac
    agent "machine-b-consumer" as mbc
}
boundary "ingress" as cds

card machine-a {
    agent "machine-a-producer" as map
}
card machine-b {
    agent "machine-b-producer" as mbp
}

control "Kafka cluster" as kc

actor "consumer-1" as c1
actor "consumer-2" as c2
actor "consumer-3" as c3
actor "consumer-4" as c4

(mac) --> (kc)
(mbc) --> (kc)

(map) -u-> (kc)
(mbp) -u-> (kc)

(c1) --> cds: offset=2
(c2) --> cds: offset=5
(c3) --> cds: offset=n/a
(c4) --> cds: offset=0
(cds) --> mac
(cds) --> mbc
@enduml
```
