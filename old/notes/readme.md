# General notes

## Kafka Consumer

+ Members of the same *consumer group* split messages from the same topic.
+ Each partition is read by one single consumer
+ For each **partition** consumers log the current offset in case of a rebalance.

## RDA server

RDA server should only care about logging data to Kafka. Each device server should be translated into a Kafka producer.

### Topics

It's common knowledge that the number of topics within a Kafka cluster should be kept limited since scaling the number of topics is not cost-free. For this reason I think the best solution for our usecase is having something like:
+ One topic per experiment/accelerator/department/project or whatever feels more reasonable.
+ Event keys are `device/property` pairs.

The solution above provides also ordering guarantees on event streaming, since events coming from the same access point are guaranteed to end-up in the same partition. However, this also means that we are not able to leverage Kafka out-of-the-box load balancing (one partition can be consumed by one consumer).

### Containerization

RDA servers are possibly (but non necessarily) containerized. For what concerns this work I assume we can model RDA servers as simple Kafka producers (TBC), without any kind of failover or fancy strategies. I also assume that RDA servers live in quite less flexible environments than RDA clients.

## RDA client - `get`

Blocking `get` could be modeled as blocking/non-blocking `subscribe` of size 1 to simplify the architecture.

## RDA client - `subscribe`

### Categories

I propose the following categorization for RDA clients:
+ *Thin* clients: no K8s, e.g. simple Java applications or terminal-based `rda-subscribe` calls.
+ *Individual* clients: need to consume all messages due to the use-case (e.g. UCAP nodes).
+ *Collaborative* clients: can share the load with peers (e.g. an hypothetical containerized complex of UCAP nodes).

### Strategy

+ Handling failure in *Thin* clients:
    + Is it needed? What's the use-case?
    + We could take system information (i.e. machine, OS, hostname, username), merge it with the device/property pair and create a unique `groupId`.
    + This gives us the possibility to reconnect and catch-up if `subscribe` failed client-side.
    + If `subscribe` is shut-down gracefully, we tell Kafka to drop the last offset logged by `groupId` to have a fresh start next time we reconnect.
        + We could provide options to force a fresh start.
+ *Individual* clients need to have a dedicated consumer group.
    + Logging the current offset relies on the `groupId` to discriminate between consumers groups, therefore it could be a k8s-managed resource.
    + Possible failover strategies:
        + As soon as K8s detects a peer died, another one takes over with the same `groupId`. The new peer should be in a "waiting" state before this happens, to reduce latency.
        + All peers in the K8s cluster are active and consume data from Kafka independently, but only one is active. When failover happens we should use some strategy (if necessary) to avoid data loss and duplication (rolling update). 
+ *Collaborative* clients need a customized strategy:
    + We can't throw them into the cluster and put all in the same Kafka consumer group, since they wouldn't be able to read from the single partition of interest.
    + It follows that each peer will need its own consumer group (same as *Individual* clients).
    + It follows that also in this case we can't leverage Kafka features (failover, load-balancing).
    + Possible strategy (ask Marcin if there's a better way to do this):
        + An intermediate layer between Kafka and the clients which pulls all events from Kafka and re-distributes with load balancing.
        + The intermediate layer is a single-point-of-failure, we should have MANY in a K8s cluster (we can share them for multiple subscriptions).
        + Clients interested in an access point register a request in the intermediate layer.
        + Clients are responsible for unsubscribing whenever they want to stop listening.
        + The subscription is dropped when no more clients from the same group are interested in the access point.
        + *Individual* clients could use the same strategy, with all peers silent except for one.

```plantuml
@startuml
left to right direction
title Idea 2

frame Server {
    card machine-1 {
        agent "cmw-server-1" as cmw1
    }
    card machine-2 {
        agent "cmw-server-2" as cmw2
    }
    card machine-3 {
        agent "cmw-server-3" as cmw3
    }
}

frame Middleware {
    card K8s as Consumers {
        agent "consumer-b" as ca
        agent "consumer-a" as cb
        agent "consumer-c" as cc
    }
    boundary "ingress" as cds
    control "Kafka cluster" as kc
}

frame Clients {
    card K8s as k1 {
        agent "peer-1" as p11
        agent "peer-2" as p12
    }
    card K8s as k2 {
        agent "peer-1" as p21
        agent "peer-2" as p22
    }
    agent "individual" as ind
}

(cmw1) -u-> (kc)
(cmw2) -u-> (kc)
(cmw3) -u-> (kc)

(ca) --> (kc)
(cb) --> (kc)
(cc) --> (kc)

(cds) --> Consumers

k1 --> (cds): "AccessPoint1"
k2 --> (cds): "AccessPoint2"
ind --> (cds): "AccessPoint3"
@enduml
```

### TBD

1. How can we persist state in the internal K8s cluster in the middleware?
    + `StatefulSet`: Official solution for stateful pods, mount persistent storage (which survives restart of the pod) and network configuration
    + `etcd`: Key/Value distributed storage with gRPC API (Java clients available)
1. How to deal with different clients asking for the same RDA access point?
    + Different middleware pods cannot co-operate on the same RDA access point, since each wants to move the Kafka offset
    + Clients should register to the same middleware pod
    + The pod should be responsible for routing information from Kafka cluster to all subscribed clients
    + Therefore we need a way to tell if a Middleware pod has already the **lock** on an RDA access point, how can we do this? 
    + `etcd`(?)
1. Fail-safe communication between middleware and **collaborative** clients?
    1. The middleware extracts data from the cluster.
    2. Data is redirected to the appropriate peer.
    3. The middleware requires an `ack` to confirm the operation was completed.
    4. When the `ack` is received the middleware commits the new offset to the cluster.
        + We must deal with the fact that `ack` could come back unordered.
        + We could buffer the acks until we get an ordered sequence, then we can write to Kafka.
        + **BUT**: this state must be secured from failure.
    5. If a peer fails to send back an `ack` before some user-configures threshold, the middleware assumes it's dead and it tries with another peer.
1. Fail-safe communication between middleware and **individual** clients?
    + We could do the same as above, with messages coming in-order and `ack`s required to confirm the message was processed.
    + We could buffer a few `ack`s before updating Kafka offset to improve performance.
1. How do clients register interest? Few options:
    1. All peers contact the middleware independently, and unregister in the same way.
    2. A leader contacts the middleware and provides means to reach its peers, and is also responsible to unregister.
    3. Maybe this can be done nicely with some K8s magic?
