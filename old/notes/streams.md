# Kafka Streams

Streams are processing facility provided by Kafka to ease running stateless and stateful operations on the content of a topic.

## Architecture

- Each client instance running Kafka Streams executes a single Kafka Stream task (spawned by some internal Kafka Stream orchestrator).
- Each Stream maps to a single topic-partition
  - Therefore the max. number of Streams we can run is the number of partitions
  - If a Stream joins when the maximum number of running instances is already reached, the Stream stays idle.
- Tasks are assigned to threads over all instances of the application, the assignment is internal and cannot be tweaked. Each thread can request one or more tasks.
- A single machine could also run multiple instances of a Kafka Stream application if needed, to fill all the stream slots.
- When a Stream application fails, all its tasks are automatically restarted on other instances from the same stream partitions.

## Fault tolerance

This may be of interest even if we decide not to pursue the Streams-way, to take inspiration from how they do it internally in the development team.

## Consequences

- Streams are not suitable to act as cmw clients: we need much more clients running in parallel than the number of partitions of a given topic.
- We cannot afford a client staying idle until a "stream slot" becomes available.
