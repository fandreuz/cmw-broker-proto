# cmw-broker meeting (March)

## Attendees

- Martin
- Francesco

## Topics

In-depth discussion about architectural challenges of the current cmw-rda implementation.

## Notes

- Server has no clue whether a client has subscribed
  - Problem solved with ZeroMQ buffer (on the server)
  - Buffer has limited capacity
  - Same if the client is slow to process (server has to buffer)
- There are two buffers
  - TCP buffer
    - No control on the parameters
  - ZeroMQ buffer
    - One buffer per connected client
    - User-defined parameters (client-side), by default it's unbounded
    - If buffer is full --> message is dropped
- **Goal of the project**
  - Introduce a way for the client to catch-up when it reconnects
  - At the moment the client has no way to tell the server where it was
    - Martin suggests this problem is solved client-side
    - State should be stored somewhere in case client fails
  - Kafka should receive data from all _pushing_ servers (3-layers architecture, with Kafka being the middleware)
- There's still ZeroMQ between CMW servers and Kafka
