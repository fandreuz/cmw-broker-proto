# cmw-broker meeting (May)

## Attendees

- Wojtek
- Martin
- Francesco

## Topics

Discussion about architecture proposal.

## Notes

+ We could have one Kafka topic per team
    + Everyone manages on their own how much they can publish
    + Nobody affects other projects for which they are not responsible
+ We need an additional layer between cmw-server and the Kafka cluster
    + We cannot redeploy cmw-servers
    + We need to provide a sink layer which publishes to Kafka for existing services
    + Proposed architecture (not usable, needs changes, just the idea)
        + `cmw-server` starts
        + It binds to the directory service **synchronously**
        + DS notifies the new sink layer
        + The sink layer subscribes to the newly started cmw-server
        + Updates are published to Kafka
        + The service gains control once we're up and running
    + Problems (TBD):
        + At the moment this is not fully usable because we cannot `subscribe` to RDA until the RDA server call
          to `bind` (to the directory service) has returned
        + The call from RDA client fails due to server not known
        + Is it retained somewhere? Is it retried automatically?
+ We need timestamp-based consuming, not offset-based
+ How to deal with cycle bound-ness?
    + 32 different streams with many timing users
    + Make sure there's still total order after the messages pass through the architecture
    + We also need to take this into account in REST API (e.g. provide appropriate endpoints with selectors)
    + Clients usually need to perform get/set/subscribe on a single user of those 32
+ We should test a scenario when client is too slow to consume and the offset becomes out of range for Kafka
+ HTTP+REST is probably the most flexible approach, otherwise we impose Java client
    + But we could also provide a Kafka-client based RDA client(TBD)
+ Sets should be forwarded to the server (simple proxy)

## TODO

- Have a look at Kafka set-up for Tracing
- Redesign the architecture based on feedback
    - Main use-case involves only thin clients
    - Individual clients can be modeled as many thin clients in different consumer groups
    - We can definitely use the Kafka Consumer API instead of providing a custom layer to distribute messages 