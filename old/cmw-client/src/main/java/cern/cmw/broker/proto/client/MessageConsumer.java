package cern.cmw.broker.proto.client;

import cern.cmw.broker.proto.client.configuration.KafkaConsumerConfiguration;
import cern.cmw.broker.proto.common.Configuration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageConsumer {

    @KafkaListener(topics = Configuration.TOPIC_NAME, groupId = KafkaConsumerConfiguration.CONSUMER_GROUP_ID)
    public void listenGroupTest(String messageReceived) {
        log.info("Received message: '{}'", messageReceived);
    }
}
