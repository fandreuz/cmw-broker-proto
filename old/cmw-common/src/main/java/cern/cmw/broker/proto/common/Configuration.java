package cern.cmw.broker.proto.common;

public final class Configuration {

    public static final String TOPIC_NAME = "test-topic";

    private Configuration() {
        throw new UnsupportedOperationException("No instances allowed");
    }
}
